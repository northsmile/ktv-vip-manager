-- 创建数据库
drop database if exists vipmanager;
create database if not exists vipmanager charset utf8mb4;
use vipmanager;
-- 创建用户表
create table userinfo(
    uid int primary key auto_increment,
    username varchar(250) not null,
    loginname varchar(250) not null unique key,
    password varchar(65) not null,
    age int default 0,
    gender varchar(2) default '男',
    address varchar(250) default '',
    qq varchar(250) default '',
    email varchar(250) default '',
    admin int default 0,
    state int default 1,
    createtime datetime default now(),
    updatetime datetime default now()
);

-- 插入初始管理员信息
insert into userinfo (username,loginname,password,admin) values ("超级管理员","admin","2e2b97bbe8f04a4d8e2602cb3232a16b$a94554c302f481d362dba9c7fd39e640",1);