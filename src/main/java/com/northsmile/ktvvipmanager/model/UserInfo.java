package com.northsmile.ktvvipmanager.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * @author NorthSmile
 * @version 1.0
 * @date 2023/3/30&21:13
 * 用户信息实体类
 */
@Data
public class UserInfo {
    private Integer uid;
    private String username;
    private String loginName;
    private String password;
    private Integer age;
    private String gender;
    private String address;
    private String qq;
    private String email;
    // 不要用is开头声明变量
    private Integer admin;
    private Integer state;
    private String createTime;
    private String updateTime;
}
