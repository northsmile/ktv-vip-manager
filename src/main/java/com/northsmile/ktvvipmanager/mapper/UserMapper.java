package com.northsmile.ktvvipmanager.mapper;

import com.northsmile.ktvvipmanager.model.UserInfo;
import org.apache.ibatis.annotations.*;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * Author:NorthSmile
 * Create:2023/3/31 16:47
 * 用户信息mapper接口
 */
@Mapper
public interface UserMapper {
    UserInfo selectByLoginName(String loginName);

    UserInfo selectByUid(Integer uid);

    UserInfo selectByLoginNameAndPassword(String loginName, String password);

    List<UserInfo> selectAll();

    // 条件查询
    List<UserInfo> selectAllByPage(String username,String address,String email,Integer pSize,Integer size);

    Integer selectAllByConditions(String username,String address,String email);

    Integer insert(UserInfo userInfo);

    Integer deleteByUid(Integer uid);

    Integer deleteByIds(List<Integer> ids);

    Integer update(UserInfo userInfo);
}
