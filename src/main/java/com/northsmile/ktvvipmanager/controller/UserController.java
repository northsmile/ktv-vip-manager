package com.northsmile.ktvvipmanager.controller;

import com.northsmile.ktvvipmanager.model.UserInfo;
import com.northsmile.ktvvipmanager.service.UserService;
import com.northsmile.ktvvipmanager.util.ConstVariable;
import com.northsmile.ktvvipmanager.util.PasswordUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * @author NorthSmile
 * @version 1.0
 * @date 2023/3/30&21:48
 * 用户操作功能控制层
 */
@Controller
public class UserController {
    @Autowired
    private UserService userService;

    @RequestMapping(value = "/login.do",method = RequestMethod.POST)
    @ResponseBody
    public Boolean login(String loginName, String password, HttpServletRequest request){
        // 合法校验
        if ((loginName!=null&&loginName.length()!=0)&&(password!=null&&password.length()!=0)){
            // 数据库中查询此用户名对应信息
            UserInfo searchUser = userService.validLoginName(loginName);
//            System.out.println(searchUser);
            // 说明可以查询到该用户
            if (searchUser!=null&&searchUser.getUid()!=0 && PasswordUtil.decrypt(password,searchUser.getPassword())) {
                System.out.println("登录时："+searchUser);
                // 确保该用户为超级管理员
                if(searchUser.getAdmin()==1) {
                    // 存储会话ID
                    HttpSession session = request.getSession(true);
                    session.setAttribute(ConstVariable.USER_SESSION_ID, searchUser);
                    return true;
                }
            }
        }
        return false;
    }

    @RequestMapping("list.do")
    @ResponseBody
    public List<UserInfo> list(){
//        System.out.println(userService.list());
        return userService.list();
    }

    // 列表页显示当前页所有用户
    @RequestMapping("listByPage.do")
    @ResponseBody
    public HashMap<String, Object> listByPage(String username, String address, String email, Integer pIndex, Integer pSize){
        // 排除特殊情况
        if(pIndex==null||pIndex<=1){
            pIndex=1;
        }
        if (pSize==null||pSize<=0){
            pSize=2;
        }
        if (!StringUtils.hasLength(username)){
            username=null;
        }
        if (!StringUtils.hasLength(address)){
            address=null;
        }
        if (!StringUtils.hasLength(email)){
            email=null;
        }
//        System.out.println("username:"+username);
//        System.out.println("address:"+address);
//        System.out.println("email:"+email);
//        System.out.println("pIndex:"+pIndex);
//        System.out.println("pSize:"+pSize);
        // 计算偏移量
        HashMap<String, Object> map = new HashMap<>();
        Integer size=(pIndex-1)* pSize;
        List<UserInfo> userList = userService.listByPage(username, address, email, pSize, size);
//        System.out.println("查询结果："+userList);
        map.put("list",userList);
        Integer count=userService.infoCounts(username, address, email);
        map.put("count",count);
        return map;
    }

    @RequestMapping("add.do")
    @ResponseBody
    public Integer add(UserInfo userInfo,HttpServletRequest request){
        System.out.println("添加："+userInfo);
        Integer result=0;
        // 1.合法校验
        // 传入参数非空
        if (userInfo!=null&& StringUtils.hasLength(userInfo.getUsername())&& StringUtils.hasLength(userInfo.getLoginName())
                && StringUtils.hasLength(userInfo.getPassword())) {
            // 2.登录名唯一性校验
            UserInfo isUniqueInfo = userService.validLoginName(userInfo.getLoginName());
            if (isUniqueInfo==null) {
                // 3.验证当前登录用户为超级管理员
                HttpSession session = request.getSession(false);
                UserInfo curUser = (UserInfo)session.getAttribute(ConstVariable.USER_SESSION_ID);
                if (session!=null&&curUser!=null&&curUser.getAdmin()==1){
                    if (userInfo.getAge()==null){
                        userInfo.setAge(0);
                    }
                    if (userInfo.getQq()==null){
                        userInfo.setQq("");
                    }
                    if (userInfo.getEmail()==null){
                        userInfo.setEmail("");
                    }
                    // 加密
                    userInfo.setPassword(PasswordUtil.encrypt(userInfo.getPassword()));
                    System.out.println("添加时："+userInfo);
                    // 用户添加
                    result = userService.add(userInfo);
                }
            }
        }
        return result;
    }

    @RequestMapping("delete.do")
    @ResponseBody
    public Integer deleteOne(Integer uid,HttpServletRequest request){
//        System.out.println("uid:"+uid);
        // 1.非空校验
        if (uid>0){
            // 2.验证待删除用户为普通用户
            UserInfo user = userService.selectByUid(uid);
//            System.out.println("待删除用户："+user);
            if (user!=null&&user.getAdmin()==0) {
                // 3. 验证当前登录用户为超级管理员
                HttpSession session = request.getSession(false);
                if (session != null) {
                    UserInfo curUser = (UserInfo) session.getAttribute(ConstVariable.USER_SESSION_ID);
                    if (curUser != null && curUser.getAdmin()==1) {
//                        System.out.println("当前用户："+curUser);
                        return userService.deleteOne(uid);
                    }
                }
            }
        }
        return 0;
    }

    @RequestMapping("delByIds.do")
    @ResponseBody
    public Integer deleteMulti(String ids,HttpServletRequest request){
        // 1.非空校验
        if (ids!=null&&ids.length()>0){
            // 2.验证待删除用户为普通用户
            String[] idsArr = ids.substring(0, ids.length() - 1).split(",");
            ArrayList<Integer> idsList=new ArrayList<>();
            for (String id:idsArr){
                Integer idN = Integer.parseInt(id);
                if (userService.selectByUid(idN).getAdmin()==1){
                    return 0;
                }
                idsList.add(idN);
            }
            System.out.println(idsList);
            // 3. 验证当前登录用户为超级管理员
            HttpSession session = request.getSession(false);
            if (session != null) {
                UserInfo curUser = (UserInfo) session.getAttribute(ConstVariable.USER_SESSION_ID);
                if (curUser != null && curUser.getAdmin()==1) {
                    return userService.deleteMulti(idsList);
                }
            }
        }
        return 0;
    }

    @RequestMapping("load.do")
    @ResponseBody
    public UserInfo loadInfo(Integer uid){
        if(uid>0){
            UserInfo userInfo = userService.selectByUid(uid);
            if (userInfo!=null){
                // 将密码置空
                userInfo.setPassword("");
                return userInfo;
            }
        }
        return null;
    }
    @RequestMapping("update.do")
    @ResponseBody
    public Integer update(UserInfo userInfo,HttpServletRequest request){
//        System.out.println(userInfo);
        // 1.非空校验
        if (userInfo!=null&& StringUtils.hasLength(userInfo.getUsername())){
            // 2.验证当前登录用户为超级管理员
            HttpSession session = request.getSession(false);
            UserInfo curUser = (UserInfo)session.getAttribute(ConstVariable.USER_SESSION_ID);
            if (session!=null&&curUser!=null&&curUser.getAdmin()==1){
                if (!StringUtils.hasLength(userInfo.getPassword())){
                    userInfo.setPassword(null);
                }
                // 加密
                userInfo.setPassword(PasswordUtil.encrypt(userInfo.getPassword()));
                return userService.update(userInfo);
            }
        }
        return 0;
    }
}
