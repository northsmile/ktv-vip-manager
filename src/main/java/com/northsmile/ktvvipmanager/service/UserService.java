package com.northsmile.ktvvipmanager.service;

import com.northsmile.ktvvipmanager.mapper.UserMapper;
import com.northsmile.ktvvipmanager.model.UserInfo;
import org.apache.catalina.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Author:NorthSmile
 * Create:2023/3/31 17:27
 * Service层
 */
@Service
public class UserService {
    @Autowired
    private UserMapper userMapper;

    // 登录功能
    public UserInfo login(String loginName,String password){
        UserInfo userInfo = userMapper.selectByLoginNameAndPassword(loginName, password);
        return userInfo;
    }

    // 登录名合法校验
    public UserInfo validLoginName(String loginName){
        return userMapper.selectByLoginName(loginName);
    }

    // 根据uid查询用户
    public UserInfo selectByUid(Integer uid){
        return userMapper.selectByUid(uid);
    }

    // 列表页显示所有用户
    public List<UserInfo> list(){
        return userMapper.selectAll();
    }

    // 列表页显示当前页所有用户
    public List<UserInfo> listByPage(String username,String address,String email,Integer pSize,Integer size){
        return userMapper.selectAllByPage(username,address,email,pSize,size);
    }

    // 列表页显示当前页所有用户
    public Integer infoCounts(String username,String address,String email){
        return userMapper.selectAllByConditions(username,address,email);
    }

    // 新增用户
    public Integer add(UserInfo userInfo){
        return userMapper.insert(userInfo);
    }

    // 删除单个用户
    public Integer deleteOne(Integer uid){
        return userMapper.deleteByUid(uid);
    }

    // 删除多个用户
    public Integer deleteMulti(List<Integer> ids){
        return userMapper.deleteByIds(ids);
    }

    // 修改用户信息
    public Integer update(UserInfo userInfo){
        return userMapper.update(userInfo);
    }
}
