package com.northsmile.ktvvipmanager;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class KtvVipManagerApplication {

	public static void main(String[] args) {

		SpringApplication.run(KtvVipManagerApplication.class, args);
	}

}
