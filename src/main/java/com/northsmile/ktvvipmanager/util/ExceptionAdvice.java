package com.northsmile.ktvvipmanager.util;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;

/**
 * @author NorthSmile
 * @version 1.0
 * @date 2023/3/30&21:42
 * 统一异常处理
 * ControllerService+ExceptionHandler实现
 */
@ControllerAdvice
public class ExceptionAdvice {
    @ExceptionHandler(Exception.class)
    @ResponseBody
    public Object exceptionHandler(Exception e){
        HashMap<String,Object> map=new HashMap<String,Object>();
        map.put("state",-1);
        map.put("data",null);
        map.put("message",e.getMessage());
        return map;
    }
}
