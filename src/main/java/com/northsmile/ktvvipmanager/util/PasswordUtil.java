package com.northsmile.ktvvipmanager.util;

import cn.hutool.core.util.IdUtil;
import cn.hutool.crypto.SecureUtil;
import org.springframework.util.StringUtils;

/**
 * Author:NorthSmile
 * Create:2023/4/6 17:08
 * 密码工具类：
 * 加密、解密
 */
public class PasswordUtil {
    // 加密
    public static String encrypt(String password){
        if (!StringUtils.hasLength(password)){
            return null;
        }
        // 唯一的盐值
        String salt = IdUtil.simpleUUID();
        // 加密
        String finalPwd=SecureUtil.md5(salt+password);
        return salt+"$"+finalPwd;
    }

    /**
     *
     * @param password 前端传入的密码
     * @param target 数据库中存储的加密过的密码
     * @return
     */
    public static Boolean decrypt(String password,String target){
        if (StringUtils.hasLength(password)&&StringUtils.hasLength(target)){
            if (target.length()==65&&target.contains("$")){
                String[] data = target.split("\\$");
                // 分隔获取盐值
                String salt = data[0];
                // 加密
                String pwd=SecureUtil.md5(salt+password);
                // 对比
                if (data[1].equals(pwd)){
                    return true;
                }
            }
        }
        return false;
    }
}
