package com.northsmile.ktvvipmanager.util;

import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * @author NorthSmile
 * @version 1.0
 * @date 2023/3/30&22:15
 * 1.自定义拦截器
 */
@Component
public class LoginInterceptor implements HandlerInterceptor {
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        // 获取会话
        HttpSession session = request.getSession(false);
        if (session!=null&&session.getAttribute(ConstVariable.USER_SESSION_ID)!=null){
            // 已经登录
            return true;
        }
        // 未登录，转到登录页面，进行登录
        response.sendRedirect("/login.html");
        return false;
    }
}
